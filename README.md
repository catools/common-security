# CATools Common

Core Automation Toolset, contains set of tools and approaches which have been found useful for any automation approaches while building automation framework.
Specially on enterprise projects with long project life time, complex requirements and lacking test documentation,
it provides a handful of already tested functionality. Our intention is to simplify facade and simplify interface for others, and to move common logic deep to this core project,
so automation test engineer can focus on project needs.    

If you find anything nice to have in this project, please send a ticket, note, or pull request.

Last version is available in maven central:  

```xml
<dependency>
    <groupId>org.catools</groupId>
    <artifactId>common-security</artifactId>
    <version>RELEASE</version>
</dependency>
```
