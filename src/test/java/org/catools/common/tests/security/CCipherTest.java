package org.catools.common.tests.security;

import org.catools.common.concurrent.tests.CBaseUnitTest;
import org.catools.common.security.CCipher;
import org.catools.common.security.CCipherException;
import org.testng.annotations.Test;
import org.testng.util.RetryAnalyzerCount;

public class CCipherTest extends CBaseUnitTest {
    private static final String ORIGINAL_STRING = "This is very super top secret information.";
    private static final String DECRYPTED_STRING = "88V7rr/B7oPh00vivqme1XZIbz/r+ZRzOq7LKCnqLeBLcSAp89C/0MVLOZmTKdNS";
    private static final String PASSWORD = "Password for very super top secret information.";

    @Test
    public void testDecrypt() {
        verify.String.equals(CCipher.decrypt(DECRYPTED_STRING, PASSWORD), ORIGINAL_STRING, "decryption works fine");
    }

    @Test(expectedExceptions = CCipherException.class)
    public void testDecrypt_PasswordIsNull() {
        verify.String.equals(CCipher.decrypt(DECRYPTED_STRING, null), ORIGINAL_STRING, "decryption works fine");
    }

    @Test(expectedExceptions = CCipherException.class)
    public void testDecrypt_InputIsNull() {
        verify.String.equals(CCipher.decrypt(null, PASSWORD), ORIGINAL_STRING, "decryption works fine");
    }

    @Test
    public void testEncrypt() {
        verify.String.equals(CCipher.encrypt(ORIGINAL_STRING, PASSWORD), DECRYPTED_STRING, "encryption works fine");
    }

    @Test(expectedExceptions = CCipherException.class)
    public void testEncrypt_PasswordIsNull() {
        verify.String.equals(CCipher.encrypt(ORIGINAL_STRING, null), DECRYPTED_STRING, "encryption works fine");
    }

    @Test(expectedExceptions = CCipherException.class)
    public void testEncrypt_InputIsNull() {
        verify.String.equals(CCipher.encrypt(null, PASSWORD), DECRYPTED_STRING, "encryption works fine");
    }
}